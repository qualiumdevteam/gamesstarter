<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package flatkingdon
 */

?>
<style>
	.izquierda{
		float:left;
	}
	
	.sociales{
		float:right;
	}
	@media(max-width: 640px){
		.izquierda{
			float:inherit;
			text-align:center;
			padding-top:20px;
			padding-bottom:10px;
		}	
		.sociales{
			float: inherit;
		}
	}

</style>
		<div class="aviso">
			<div class="game">GamesStarter ©<?=date('Y')?></div>
			<div class="row">
				<div class="izquierda">
				<?php 
				$letra;
				$aviso;
				if(ICL_LANGUAGE_CODE== 'es'):
					$letra = '/aviso-de-privacidad';
					$aviso = 'Aviso Privacidad';
				else: 
					$letra = '/en/privacy-policy/';
					$aviso = 'Privacy Policy';
				endif;
				?>				
					<a href="<?=site_url().$letra?>"><?=$aviso?></a>
				</div>
				<div class="sociales">					
					<a href="https://www.facebook.com/pages/Games-Starter/743120162398240" class="face" target="_blank"> <i class="flaticon-facebook51"></i> </a>
					<a href="https://twitter.com/gamesstartercom" class="twitter" target="_blank"> <i class="flaticon-twitter44"></i> </a>
				</div>
			</div>
		</div>
</section>
</div>
<?php get_sidebar(); ?>
<?php wp_footer(); ?>

</body>
</html>
