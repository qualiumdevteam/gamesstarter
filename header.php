<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package flatkingdon
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>
<?php $url = get_template_directory_uri(); ?>
<style type="text/css">
	.desk{
		padding-right: 60px;
	}
</style>
<body <?php body_class(); ?>>
<?php 
	$link = site_url();
	$link .= ( (ICL_LANGUAGE_CODE == 'es') ? '' : '/en' );
?>
<div class="wrapper">
	<nav class="tab-bar">
	 	<a id="iconmovil" class="menu-icon">
  		<span></span></a>
  		<a href="<?=$link?>">
			<img src="<?=$url?>/img/gamestarter.png">
		</a>
		<?php 
	      	wp_nav_menu( 
		    	array(  'theme_location' => 'primary',
		    			'container_class' => 'movil oculto',
		    			'menu_class'=> 'side-left'		    				
		    	) 
		    );
		?>
	</nav>
	<div id="content-menu-1" class="clearfix hide-for-medium hide-for-small">
	<div class="columns large-2 medium-2">
		<a href="<?=$link?>">
			<img src="<?=$url?>/img/gamestarter.png">
		</a>
	</div>
		<?php 	
			wp_nav_menu( 
				array(  'theme_location' => 'primary',
						'menu_id' => 'menu-1',
						'menu_class'=> 'medium-block-grid-6 large-block-grid-6', 
						'container_class'=>"columns large-9 large-offset-1 medium-10 desk"
				) 
			); 
		?>
	</div>
	<section class="el">
	<div class="telon no-show"></div>