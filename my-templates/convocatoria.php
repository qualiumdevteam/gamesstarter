<?php /* Template Name: convocatoria */?>
<?php get_header() ?>
<?php 
	$url = get_template_directory_uri(); 
?>
<style>
	h1{
		color:#01A28A;
		font-family:gotham_htfmedium;
		text-align:center;
	}
	.center{
		max-width:70em;
		padding:10px;
	}
</style>
<div class="convocatoria text-center">
	<section>
		<div class="over">
			<div class="center">
				<?php include "while.php"; ?>
				<br>			
				<a href="#2" class="btn-red text-center" style="line-height:2;">
					<?php 
						echo (ICL_LANGUAGE_CODE=='es') ? '¿Cómo funciona?':
						'How does it work?';
					?>
				</a>
			</div>
			<a id="bons" href="#2"><img class="bounce" src="<?=$url?>/img/down.png"></a>
		</div>
	</section>

	<section id="2">
		<div class="steps">1</div>
		<div class="center">
			<?=get_post_meta( get_the_ID(), 'seccion2', true)?>
		</div>
		<a href="#3">
			<img class="gema izq bounce" src="<?=$url?>/img/gema2.png">
		</a>
	</section>

	<section id="3">
		<div class="steps">2</div>
		<div class="center">
			<?=get_post_meta( get_the_ID(), 'seccion3', true)?>
		</div>
		<a href="#4">
			<img class="gema der bounce" src="<?=$url?>/img/gema2.png">
		</a>
	</section>

	<section id="4">
		<div class="steps">3</div>
		<div id="list" class="center">
			<?=get_post_meta( get_the_ID(), 'cuatroseccion', true)?>
		</div>
		<a href="#5">
			<img class="gema izq bounce" src="<?=$url?>/img/gema2.png">
		</a>
	</section>

	<section id="5">
		<div class="steps">4</div>		
		<div class="center" id="list4">
			<?=get_post_meta( get_the_ID(), 'seccion5', true)?>
		</div>
		<a href="#6">
			<img class="gema der bounce" src="<?=$url?>/img/gema2.png">
		</a>
	</section>

	<section id="6">
		<div class="steps">5</div>
		<div class="center">
			<?=get_post_meta( get_the_ID(), 'seccion6', true)?>
			<br><br class="hide-for-small">			
			<a href="#7"class="btn-red text-center">
				<?php 
				echo (ICL_LANGUAGE_CODE=='es') ?
				'Quiero enviar mi proyecto':
				'I want to send my project';
				?>
			</a>
		</div>
		<a href="#7">
			<img class="gema izq bounce" src="<?=$url?>/img/gema2.png">
		</a>
	</section>

	<section id="7">
		<div class="row">
			<br class="hide-for-small"><br class="hide-for-small">
			<h1 class="title">
				<?php $form;
				if(ICL_LANGUAGE_CODE== 'es') :
					echo 'Ingresa tus datos y únete';
					$form = '[contact-form-7 id="46" title="convocatoria"]';
				else:
					echo 'Enter your information and join';
					$form = '[contact-form-7 id="182" title="convocatoriaen"]';
				endif;
				?>
			</h1><br><br>
			<div class="columns large-10 offset-1 medium-10 medium-offset-1 small-12 text-center">
				<?=do_shortcode($form)?>				
			</div>
		</div>
	</section>
</div>
<?php get_footer() ?>
