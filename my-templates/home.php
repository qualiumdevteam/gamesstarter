<?php /* Template Name: home*/ ?>
<?php get_header(); $url = get_template_directory_uri();?>
<div class="home">
	<section id="home" class="homes1 efect2" style="z-index:5;">
		<div class="over text-center">
			<div class="center">
				<?php include "while.php"; ?>
				<a href="#2" class="btn-red">
					<?=(( ICL_LANGUAGE_CODE == 'es') ? 'Más Información' : 'More information')?>
				</a>				
			</div>
			<div class="clearfix"></div>
		</div>
		<a id="bons" href="#2"><img class="bounce" src="<?=$url?>/img/down.png"></a>
	</section>
	<section id="2" class="imgtip">
		<h1 class="title text-center">
		<?=(( ICL_LANGUAGE_CODE == 'es')?'Proyectos en proceso':'Projects in process')?></h1>
		<div class="row">
			<div class="columns large-12 text-center" ng-app="DemoApp">
				<?php 
					$query = new WP_Query(array(
						'order'       => 'DESC',
						'post_type'   => 'proyectos'));				 
				?>
				<div class="carousel-demo" ng-controller="DemoCtrl">
	                <ul rn-carousel rn-carousel-index="carouselIndex3" rn-carousel-transition="hexagon" rn-carousel-buffered class="carousel3">
	                	<?php $cont=0; while($cont<3):?>
	                	<?php while($query->have_posts()): $query->the_post(); ?>
				 			<?php $img = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) ); ?>
				 			<li>
				 				<a href="<?php the_permalink() ?>">

				 					<div style="background:url(<?=$img?>)" class="bgimage">				 						
				 					</div>
				 					<h5 style="color:#01A28A;font-family:gotham_htfmedium;"><?php the_title() ?></h5>
				 				</a>        			
				 			</li>
				 		<?php endwhile;?>
				 		<?php $cont++; endwhile;?>
	 				</ul>
	 				<div id="carousel-left"><img ng-click="prev()" src="<?=$url?>/img/arrow.png" /></div>
      				<div id="carousel-right"><img ng-click="next()" src="<?=$url?>/img/arrow.png" /></div>
	 			</div>
	 		</div>
		</div>
	</section>
	<section id="tips" class="homes2">			
		<div class="row">
			<div class="childtips columns large-8 large-offset-2">
				<?php get_testimonios() ?>				
			</div>			
		</div>
	</section>	
	<?php include "tipines.php"; ?>		
</div>
<?php get_template_part( 'my-templates/fondearmivideojuego', get_post_format() );?>
<script>
	function start_angular(){
        angular.module('DemoApp', ['angular-carousel'])
        .controller('DemoCtrl', function($scope) {
        	   
        		$scope.carouselIndex3 = 5;
        	   	$scope.next = function(){
        	   		$scope.carouselIndex3 +=1;	
        	   	}
        	   	$scope.prev = function(){
        	   		$scope.carouselIndex3 -=1;	
        	   	}
        	   
        });
    }
    window.onload = start_angular();
</script>
<?php get_footer(); ?>