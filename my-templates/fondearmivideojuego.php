<?php $url = get_template_directory_uri();?>
<style>
	.gifs{
	    width: 300px;
	    height: 177px;
	    z-index: 111;
	    position: absolute;
	    top: 50px;
	    left: 30px;
	}
	.gifs img{
    	width: 80px;
    	bottom:0;
    	position: absolute;
	}
	.gifs img:nth-of-type(1){
		left:50;
	}
	.gifs img:nth-of-type(2){
		left:100px;
	}
	.gifs img:nth-of-type(3){
		left:200px;
	}
	@media(max-width:1000px){
		.gifs{
			top:-10px;
		}
	}
</style>
<section class="content-fondear">
	<?php
	$texto=''; $link='';
		if(ICL_LANGUAGE_CODE == 'es'){
			$texto = '¡Quiero financiar mi videojuego!';
			$link = '/incubadora-de-videojuegos';
		}else{
			$texto = 'I want to fund my game!';
			$link = '/incubator';
		}
	?>
	<a class="btn-red fondear" href="<?=site_url().$link?>">
		<?=$texto?>
	</a>
	<!-- <div class="gifs">
		<img src="<?//=$url?>/img/flatman.gif">
		<img src="<?//=$url?>/img/gatapult.gif">
		<img src="<?//=$url?>/img/realm.gif">
	</div> -->
	<img src="<?=$url?>/img/game-starter.gif">
</section>