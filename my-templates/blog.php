<?php /* Template Name: blog */?>
<?php get_header() ?>
<?php $url = get_template_directory_uri(); ?>
<div class="blog">
	<?php 
		get_template_part( 'template-parts/headerblog', get_post_format() );
		$cont=1; 
		$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
		$query = new WP_Query(array(
					'post_type'=>'post','p',
					'posts_per_page' => 6,
    				'paged' => $paged,
					)
				);
	
		while($query->have_posts()): $query->the_post(); 
		$clase = ( ($cont==$query->post_count) ? ' no-line':'' );
		$img = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) ); 
	?>
	<div class="row">
		<div class="columns large-3 medium-3 small-12 lined text-center<?=$clase?>">
			<a href="<?php the_permalink() ?>"><div class="foto" style="background:url(<?=$img?>)"></div></a>
		</div>
		<div class="columns large-9 medium-9 small-12">
			<a href="<?php the_permalink() ?>"><h5 class="title"><?php the_title() ?></h5>
			<p style="color:black;"><?=get_post_meta( $post->ID, 'subtitulo', true)?></p></a>
			<span class="gris">Publicado el</span>
			<span class="verde"><?=get_the_time('j \DE\ F \DE\ Y')?></span> 
			<a href="<?php the_permalink() ?>#comentarios" class="btn-red coments">... <div>
				<?php if(ICL_LANGUAGE_CODE == 'es'){ echo 'Ver comentarios';}
				else{ echo 'View Comments'; } ?>
			</div></a>
			<a href="<?php the_permalink() ?>" class="more text-center right">
				<?php if(ICL_LANGUAGE_CODE == 'es'){ echo 'Leer más';}
				else{echo 'Read More';}?>
			</a>			
		</div>
	</div>
	<?php $cont++; endwhile;?>	
	<div class="paginacion text-center"><?php  get_pagination($query) ?></div>
	<div class="clearfix"></div>
</div>
<?php get_footer() ?>