<?php /* Template Name: contacto */?>
<?php get_header() ?>
<div class="contacto">
	<div class="capa">
		<div class="row center">
			<?php if( have_posts() ): ?>
			<?php while( have_posts() ) : the_post(); ?>
			<div class="columns large-6 medium-6 small-12">
			<h1 class="title"><?php the_title() ?></h1>
			<?php the_content() ?>
			</div>
			<div class="columns large-6 medium-6 small-12">
				<div id="mapa"></div>
				<p><?=nl2br(get_post_meta( $post->ID,'seccion2', true ))?></p>
			</div>
			<?php endwhile; ?>
			<?php endif;?>
		</div>
	</div>
</div>
<?php get_footer() ?>