<?php /* Template Name: nosotros */?>
<?php get_header() ?>
<?php $url = get_template_directory_uri(); ?>
<div class="nosotros">
	<section id="home" class="homes1">
			<div class="over text-center">
				<div class="center">
					<?php include "while.php"; ?>
				</div>
				<a id="bons" href="#2"><img class="bounce" src="<?=$url?>/img/down.png"></a>
			</div>
	</section>
	<section id="2">
		<?php wp_reset_query(); ?>
		<?php
		 $team = new WP_Query(array(
				'orderby'          => 'date',
				'order'            => 'DESC',
				'post_type'        => 'equipo',
				'post_status'      => 'publish',

			));
		
			 ?>

		<?php while( $team->have_posts() ): $team->the_post(); ?>
		<div class="row masters">
			<div class="columns large-3 medium-3 small-12">
				<div class="borde">
				<?php $img = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ) ?>
					<div class="foto" style="background:url(<?=$img?>)"></div>			
				</div>
			</div>
			<div class="columns large-9 medium-9 small-12 content">
				<h3 class="title"><?php the_title() ?></h3>
				<?php the_content() ?>
			</div>
		</div>
		<?php endwhile; ?>
	</section>
	<br><br>
	<?php get_template_part( 'my-templates/fondearmivideojuego', get_post_format() );?>
</div>
<?php get_footer() ?>