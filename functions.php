<?php
/**
 * flatkingdon functions and definitions.
 *
 * @link https://codex.wordpress.org/Functions_File_Explained
 *
 * @package flatkingdon
 */

if ( ! function_exists( 'flatkingdon_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function flatkingdon_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on flatkingdon, use a find and replace
	 * to change 'flatkingdon' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'flatkingdon', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'flatkingdon' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'flatkingdon_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // flatkingdon_setup
add_action( 'after_setup_theme', 'flatkingdon_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function flatkingdon_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'flatkingdon_content_width', 640 );
}
add_action( 'after_setup_theme', 'flatkingdon_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function flatkingdon_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'flatkingdon' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'flatkingdon_widgets_init' );

function maps_api() {
    wp_register_script( 'maps-google', 'http://maps.googleapis.com/maps/api/js?sensor=false');
}
add_action('init', 'maps_api');
/**
 * Enqueue scripts and styles.
 */
function flatkingdon_scripts() {
	wp_enqueue_script( 'flatkingdon-foundation', get_template_directory_uri() . '/js/foundation.min.js', array('jquery'), '20120206', true );
	wp_enqueue_style( 'flatkingdon-fonts', get_template_directory_uri().'/fonts/fonts.css' );
	wp_enqueue_style( 'flatkingdon-foundationcss', get_template_directory_uri().'/css/foundation.min.css' );
	wp_enqueue_style( 'flatkingdon-style', get_stylesheet_uri() );

	wp_enqueue_script( 'maps-google' );

	/* bloks background */
	$url = sprintf("%s%s%s","http://",$_SERVER['HTTP_HOST'],$_SERVER['REQUEST_URI']);
	if( $url == 'http://gamesstarter.com/' || $url == 'http://gamesstarter.com/en/'){
		wp_enqueue_script( 'flatkingdon-three', get_template_directory_uri() . '/js/plugins/three.js',array(),'1.0.0',true );
		wp_enqueue_script( 'flatkingdon-threemax', get_template_directory_uri() . '/js/plugins/threemax.js',array(),'1.0.0',true );
		wp_enqueue_script( 'flatkingdon-block', get_template_directory_uri() . '/js/plugins/block.js',array(),'1.0.0',true );

		wp_enqueue_script( 'angular', get_template_directory_uri() . '/js/angular.js');
		wp_enqueue_script( 'angulartouch', get_template_directory_uri() . '/js/angular-touch.js');
		wp_enqueue_script( 'angularcarousel', get_template_directory_uri() . '/js/angular-carousel.js');

		wp_enqueue_style( 'angularcss', get_template_directory_uri().'/css/angular-carousel.min.css' );
		wp_enqueue_style( 'demoangularcss', get_template_directory_uri().'/css/demo.css' );
	
		// wp_enqueue_script( 'flatkingdon-carousel', get_template_directory_uri() . '/js/featurecarousel.min.js',array(),'1.0.0',true );
		// wp_enqueue_style('feature-carousel', get_template_directory_uri().'/css/feature-carousel.css' );
	}else{
		wp_enqueue_script( 'flatkingdon-flickityjs', get_template_directory_uri() . '/js/plugins/carousel/flickity.js',array(),'1.0.0',true );
		wp_enqueue_style( 'flatkingdon-flickitycss', get_template_directory_uri().'/js/plugins/carousel/flickity.css' );
	}
	if($url == 'http://gamesstarter.com/nosotros/' || $url == 'http://gamesstarter.com/en/about/'){
		wp_enqueue_script( 'flatkingdon-bs', get_template_directory_uri() . '/js/nosotros.js',array(),'1.0.0',true );
	}
	
	
	wp_enqueue_script( 'flatkingdon-main', get_template_directory_uri() . '/js/main.js', true );
	wp_enqueue_style( 'flatkingdon-iconcss', get_template_directory_uri().'/icons/flaticon.css' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'flatkingdon_scripts' );

function j_share(){
	if ( function_exists( 'sharing_display' ) ) 
   	{
    	return sharing_display( '', false );
	}
} 

function get_testimonios(){
	$name = ( (ICL_LANGUAGE_CODE == 'es') ? 'inicio' : 'home' );
	$args = array(
				'orderby'          => 'date',
				'order'            => 'ASC',
				'post_type'        => 'testimonios',
				'category_name'    => $name,
				'post_status'      => 'publish',
				'suppress_filters' => true 
			);				
	
	foreach(get_posts( $args ) as $k):
		echo $k->post_content.'<br>';
	endforeach;
}
function get_equipo(){
	$args = array(
				'orderby'          => 'date',
				'order'            => 'DESC',
				'post_type'        => 'equipo',
				'post_status'      => 'publish',

			);				
				// 'suppress_filters' => true 
	// return get_posts( $args );
	return new WP_Query($arg);

}
function rel_posts(){
    global $post;
    $tag_ids = array();
    $current_cat = get_the_category($post->ID);
    $current_cat = $current_cat[0]->cat_ID;
    $this_cat = '';
    $tags = get_the_tags($post->ID);
    $contx=0;

	if ( $tags ){
        foreach($tags as $tag){
            $tag_ids[] = $tag->term_id;
        }
    }else{
        $this_cat = $current_cat;
    }
    $args = array(
            'post_type'   => get_post_type(),
            'numberposts' => 4,
            'orderby'     => 'rand',
            'tag__in'     => $tag_ids,
            'cat'         => $this_cat,
            'exclude'     => $post->ID             
    );

	$related_posts = get_posts($args);
    /**
     * If the tags are only assigned to this post try getting
     * the posts again without the tag__in arg and set the cat
     * arg to this category.
     */
    if ( empty($related_posts) ) {
        $args['tag__in'] = '';
        $args['cat'] = $current_cat;
        $related_posts = get_posts($args);
    }
	if ( empty($related_posts) ) {
    	return;
    }
   if ( empty($related_posts) ) {
                return;
    }
        
    foreach($related_posts as $related) {
      	echo "<a href=".get_permalink($related->ID)." class='item cell' style='background:url(".wp_get_attachment_url( get_post_thumbnail_id( $related->ID ) ).")'>";
        echo "<h3 class='text-center'>".$related->post_title."</h3>";//the_title( "<h3 class='text-center'>", "</h3>" );    	
    	echo '</a>';
    }    
    
}//fin rel_posts();

// add_action( 'wp_ajax_nopriv_get_comenta', 'get_comenta' );
// add_action( 'wp_ajax_get_comenta', 'get_comenta' );
// function get_comenta()
// {
// 	global $wpdb;
// 	$query = "SELECT * FROM  `fk_comments` WHERE `fk_comments`.`comment_post_ID` =1";
// 	$query = $wpdb->get_results($query, ARRAY_A);
// 	echo json_encode($query); exit();
// }
function get_pagination($query){
    $big = 999999999; // need an unlikely integer
    echo paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        //'base' => @add_query_arg('page','%#%'),
        'format' => '?paged=%#%',
        'prev_text'    => __('«'),
        'current' => max(1,get_query_var('paged') ),
        'next_text'    => __('»'),
        'total' => $query->max_num_pages,
    ) );
}
/**
 * Registro de los posts type proyectos y convocatorias
 */
require get_template_directory() . '/inc/posts_type.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
