<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package flatkingdon
 */
#373670 bakground
#222242 imagen destacada
get_header(); 
$query = new WP_Query(array('post_type'=>array('post','proyectos'),'s'=> get_query_var('s')));
// var_dump($query); die();
?>

	<section id="primary" class="content-area">
		<main id="main" class="row site-main" role="main">

		<?php if ( $query->have_posts() ) : ?>

			<header class="page-header">
				<h1 class="title"><?php printf( esc_html__( 'Resultados para: %s', 'flatkingdon' ), '<span>' . $query->get_search_query() . '</span>' ); ?></h1>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( $query->have_posts() ) : $query->the_post(); ?>

				<?php
				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );
				?>

			<?php endwhile; ?>

			<?php the_posts_navigation(); ?>

		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
