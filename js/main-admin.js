jQuery(document).ready(function() {
    jQuery('.color-field').wpColorPicker();

    var formfield;

    /* user clicks button on custom field, runs below code that opens new window */
    var custom_uploader;
    var images = [];
    var images_re = [];
    var currentImages;
    var position;
    jQuery('.onetarek-upload-button').click(function(e) {
        var contador = parseInt( jQuery(this).data('contador') );
        e.preventDefault();
        //If the uploader object has already been created, reopen the dialog
        if (custom_uploader) {
            custom_uploader.open();
            return;
        }

        //Extend the wp.media object
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image',
            button: {
                text: 'Choose Image'
            },
            multiple: true
        });

        //When a file is selected, grab the URL and set it as the text field's value
        custom_uploader.on('select', function() {
            attachment = custom_uploader.state().get('selection').toJSON();
            var value = jQuery('#image_location').val();
            if(value != ""){
                currentImages = JSON.parse(value);
                images = currentImages;
            }
            // sizes : {
            //     thumbnail : val.sizes.thumbnail.url,
            //     medium : val.sizes.medium.url,
            //     full : val.sizes.full.url
            // }
            jQuery.each( attachment, function( i, val ) {
                var sizes = {
                        image : val.name,
                        full : val.sizes.full.url
                    };
                //create images sizes array
                // position = parseInt(contador) + i;
                images.push(sizes);
                jQuery('.items').append('<div class="gaimg img_'+i+'"><img src="'+val.sizes.thumbnail.url+'" ><a data-position="'+i+'" class="remove_img" name="'+ val.name +'">Borrar</a></div>');
            });            

            jQuery('#image_location').val(JSON.stringify(images));
            jQuery(".remove_img").click(function(){
                var position=parseInt(jQuery(this).data('position'));
                delete images[position]
                // jQuery('.img_' + position + '').hide('slow');
                //Eliminamos el div que contiene la imagen
                jQuery(this).parent().remove();
                jQuery('#image_location').val(JSON.stringify(images));
            });
        });

        //Open the uploader dialog
        custom_uploader.open();

    });

    jQuery(".remove_img").click(function(){
        var arr = JSON.parse(jQuery('#image_location').val());
        var position=parseInt(jQuery(this).data('position'));
        arr.splice(position,1);
        // delete arr[position]
        console.log(arr);
        // jQuery('.img_' + position + '').hide('slow');
        jQuery(this).parent().remove();
        jQuery('#image_location').val(JSON.stringify(arr));
        
    });


    /*
     Please keep these line to use this code snipet in your project
     Developed by oneTarek http://onetarek.com
     */
    //adding my custom function with Thick box close function tb_close() .
    window.old_tb_remove = window.tb_remove;
    window.tb_remove = function() {
        window.old_tb_remove(); // calls the tb_remove() of the Thickbox plugin
        formfield=null;
    };

    // user inserts file into post. only run custom if user started process using the above process
    // window.send_to_editor(html) is how wp would normally handle the received data

    window.original_send_to_editor = window.send_to_editor;
    window.send_to_editor = function(html){
        if (formfield) {
            fileurl = jQuery('img',html).attr('src');
            jQuery(formfield).val(fileurl);
            tb_remove();
        } else {
            window.original_send_to_editor(html);
        }
    };
});