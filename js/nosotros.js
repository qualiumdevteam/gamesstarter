~ function() {


	'use strict';

	// ---- main loop ----
	function run() {

		requestAnimationFrame(run);
		ctx.clearRect(0, 0, canvas.width, canvas.height);
		world.render();
	
	}

	// ---- easing ----

	function Ease (speed, val) {
		this.speed = speed;
		this.target = val;
		this.value = val;
	}

	Ease.prototype.ease = function (target) {
		this.value += (target - this.value) * this.speed;
	}

	// ---- world constructor ----

	function World (size, width) {

		this.size       = size;
		this.width      = width;
		this.cellWidth  = this.width / this.size;
		this.count      = size * size * size;

		// ---- trigo ----

		this.cosY       = 0.0;
		this.sinY       = 0.0;
		this.cosX       = 0.0;
		this.sinX       = 0.0;
		this.cosZ       = 0.0;
		this.sinZ       = 0.0;
		this.angleY     = new Ease(0.1, 0);
		this.angleX     = new Ease(0.1, 0);
		this.angleZ     = new Ease(0.1, 0);
		this.autoZ      = 0;

		// ---- focal ----

		this.fl         = canvas.width / 5;
		this.zEye       = new Ease(0.1, 0);
		pointer.scale   = this.size;

		// ---- points ----

		this.points     = [];
		this.iPoints    = [];

		// ---- create new point ----

		this.newPoint = function (x, y, z, project) {
			var s = (this.size) * 2;
			var index = (x + this.size) + (y + this.size) * s + (z + this.size) * s * s;
			var p = this.iPoints[index];
			if (p) return p;
			p = new Point(this, x, y, z, project);
			this.points.push(p);
			this.iPoints[index] = p;
			return p;
		}

		// ---- projection loop ----

		this.points.project = function () {
			for (var i = 0, len = this.length; i < len; i++) {
				this[i].project();
			}
		}

		// ---- volume ----

		this.volume     = [];
		for (var i = 0; i < this.count; i++) this.volume[i] = 0;
		this.volume.size = this.size;

		this.volume.get = function (x, y, z) {
			return this[x + y * this.size + z * this.size * this.size];
		};

		this.volume.set = function (x, y, z, value) {
			this[x + y * this.size + z * this.size * this.size] = value;
		};

		// ---- faces ----

		this.faces      = [];
		this.faces.world = this;

		// ---- insertion sort ----

		this.faces.sort = function () {
			for (var i = 0, len = this.length; i < len; i++) {
				var j = i, item = this[j];
				for(; j > 0 && this[j - 1].zIndex < item.zIndex; j--) {
					this[j] = this[j - 1];
				}
				this[j] = item;
			}
		};

		// ---- culling loop ----

		this.faces.culling = function () {
			for (var i = 0, len = this.length; i < len; i++) {
				var f = this[i];
				if ( f.isVisible() ) {
					var dx = f.center.x;
					var dy = f.center.y;
					var dz = f.center.z + this.world.zEye.value;
					f.zIndex = (dx * dx + dy * dy + dz * dz);
				}
			}
		};

		// ---- draw loop ----

		this.faces.draw = function () {
			for (var i = 0, len = this.length, face; i < len; i++) {
				if (this[i].visible) this[i].draw(ctx);					
				else break;
			}
		};

	}

	// ---- build world ----

	World.prototype.build = function () {

		var seed = 1965;
		function random() {
			seed = (seed * 31415821 + 1) % 1E8;
			return  seed / 1E8;
		}

		// ---- set volume ----

		var m = this.size / 2;

		for (var x = 1; x < this.size-1; x++) {
			for (var y = 1; y < this.size-1; y++) {
				for (var z = 2; z < this.size-2; z++) {
					if (x > m - 4 && x < m + 4 && y > m - 4 && y < m + 4 && z > m - 4 && z < m + 4) {
						// the light
					} else {
						this.volume.set(x,y,z, random() > 0.98 ? 47 : 0);
					}
					this.volume.set(Math.round(+this.size / 4), Math.round(+this.size / 4), z, 215);
					this.volume.set(Math.round(+this.size / 4), Math.round(-this.size / 4), z, 215);
					this.volume.set(Math.round(-this.size / 4), Math.round(+this.size / 4), z, 215);
					this.volume.set(Math.round(-this.size / 4), Math.round(-this.size / 4), z, 215);
				}
				if (x > 3 && x < this.size-3 && y > 3 && y < this.size-3) {
					this.volume.set(x, y, 1, 215); 
				}
			}
		}

		var m = this.size / 2;
		for (var x = m - 2; x < m + 2; x++) {
			for (var y = m - 2; y < m + 2; y++) {
				for (var z = m - 2; z < m + 2; z++) {
					this.volume.set(x, y, z, 360);
				}
			}
		}


		// ---- build faces ----

		var value;
		for (var x = 0; x < this.size; x++) {
			for (var y = 0; y < this.size; y++) {
				for (var z = 0; z < this.size; z++) {
					if (this.volume.get(x, y, z) === 0) {
						value = this.volume.get(x-1, y, z);
						if (value > 0) this.faces.push(new Face(this,
							2*x-1, 2*y-1, 2*z+1,
							2*x-1, 2*y+1, 2*z+1, 
							2*x-1, 2*y+1, 2*z-1,
							2*x-1, 2*y-1, 2*z-1, 
							-1, 0, 0, value
						));
						value = this.volume.get(x+1, y, z);
						if (value > 0) this.faces.push(new Face(this,
							2*x+1, 2*y+1, 2*z-1,
							2*x+1, 2*y+1, 2*z+1, 
							2*x+1, 2*y-1, 2*z+1,
							2*x+1, 2*y-1, 2*z-1, 
							1, 0, 0, value
						));
						value = this.volume.get(x, y-1, z);
						if (value > 0) this.faces.push(new Face(this,
							2*x+1, 2*y-1, 2*z-1,
							2*x+1, 2*y-1, 2*z+1, 
							2*x-1, 2*y-1, 2*z+1,
							2*x-1, 2*y-1, 2*z-1, 
							0, -1, 0, value
						));
						value = this.volume.get(x, y+1, z);
						if (value > 0) this.faces.push(new Face(this,
							2*x-1, 2*y+1, 2*z+1,
							2*x+1, 2*y+1, 2*z+1, 
							2*x+1, 2*y+1, 2*z-1,
							2*x-1, 2*y+1, 2*z-1, 
							0, 1, 0, value
						));
						value = this.volume.get(x, y, z-1);
						if (value > 0) this.faces.push(new Face(this,
							2*x-1, 2*y+1, 2*z-1,
							2*x+1, 2*y+1, 2*z-1, 
							2*x+1, 2*y-1, 2*z-1,
							2*x-1, 2*y-1, 2*z-1, 
							0, 0, -1, value
						));
						value = this.volume.get(x, y, z+1);
						if (value > 0) this.faces.push(new Face(this,
							2*x+1, 2*y-1, 2*z+1,
							2*x+1, 2*y+1, 2*z+1, 
							2*x-1, 2*y+1, 2*z+1,
							2*x-1, 2*y-1, 2*z+1, 
							0, 0, 1, value
						));
					}
				}
			}
		}
		
	};

	// ---- render world

	World.prototype.render = function () {

		// ---- view angles ----
		this.angleX.ease(Math.PI / 2 + pointer.dy / 200);
		this.angleZ.ease(-pointer.dx / 200 + (this.autoZ -= 0.002));
		this.zEye.ease(pointer.scale);
		if (pointer.scale < 0) pointer.scale += -pointer.scale / 100;

		// ---- cosin ----
		this.cosY = Math.cos(this.angleY.value + 0.001);
		this.sinY = Math.sin(this.angleY.value + 0.001);
		this.cosX = Math.cos(this.angleX.value + 0.001);
		this.sinX = Math.sin(this.angleX.value + 0.001);
		this.cosZ = Math.cos(this.angleZ.value + 0.001);
		this.sinZ = Math.sin(this.angleZ.value + 0.001);

		// ---- render ---
		this.points.project();
		this.faces.culling();
		this.faces.sort();
		this.faces.draw();

	}

	// ---- faces constructor ----

	function Face (world, x0, y0, z0, x1, y1, z1, x2, y2, z2, x3, y3, z3, nx, ny, nz, color) {

		var o        = -world.size;
		this.ctx     = ctx;
		this.world   = world;
		this.p0      = world.newPoint(o+x0, o+y0, o+z0, true);
		this.p1      = world.newPoint(o+x1, o+y1, o+z1, true);
		this.p2      = world.newPoint(o+x2, o+y2, o+z2, true);
		this.p3      = world.newPoint(o+x3, o+y3, o+z3, true);
		this.normal  = world.newPoint(nx, ny, nz, false);
		this.center  = world.newPoint((o+x0 + o+x1 + o+x2 + o+x3) / 4, (o+y0 + o+y1 + o+y2 + o+y3) / 4, (o+z0 + o+z1 + o+z2 + o+z3) / 4, true);
		this.color   = color;
		this.light   = (this.color === 360);
		this.visible = true;
		this.zIndex  = 0;
		var dx       = this.center.x / world.size * 0.5;
		var dy       = this.center.y / world.size * 0.5;
		var dz       = this.center.z / world.size * 0.5;
		this.distance = Math.sqrt(dx * dx + dy * dy + dz * dz);

	}

	Face.prototype.isVisible = function () {

		if (this.p0.visible && this.p1.visible && this.p2.visible && this.p3.visible) {
			// ---- back face culling ----
			if (
				(this.p1.yp - this.p0.yp) / (this.p1.xp - this.p0.xp) < (this.p2.yp - this.p0.yp) / (this.p2.xp - this.p0.xp) 
				^ this.p0.xp < this.p1.xp === this.p0.xp > this.p2.xp
			) {
				// ---- face visible ----
				this.visible = true;
				return true;
			}
		}
		// ---- face hidden ----
		this.visible  = false;
		this.zIndex = -99999;
		return false;

	};

	Face.prototype.lineTo = function (p0) {

		// ---- draw line ----

		var x = (this.center.xp - p0.xp);
		var y = (this.center.yp - p0.yp);
		var d = 1 / Math.sqrt(x * x + y * y);
		this.ctx.lineTo(p0.xp - x * d, p0.yp - y * d);

	}

	Face.prototype.draw = function () {

		// ---- draw face ----

		ctx.beginPath();
		this.lineTo(this.p0);
		this.lineTo(this.p1);
		this.lineTo(this.p2);
		this.lineTo(this.p3);

		// ---- lightning ----

		var c = this.light ? 100 : (100 - this.distance * 200) * (1 - this.normal.z) + Math.max(0, 30 - this.zIndex * 0.1);
		ctx.fillStyle = 'hsl(' + this.color + ','+ (70) + '%,' + (c) +'%)';
		ctx.fill();

	};

	// ---- point constructor ----

	function Point (world, x, y, z, project) {

		this.world     = world;
		this.project2D = project;
		this.visible   = false;
		this.x         = x;
		this.y         = y;
		this.z         = z;
		this.xo        = x;
		this.yo        = y;
		this.zo        = z;
		this.xp        = 0.0;
		this.yp        = 0.0;

	}

	Point.prototype.project = function () {

		// ---- 3D rotation ----

		var u  = this.world.sinZ * this.yo + this.world.cosZ * this.xo;
		var t  = this.world.cosZ * this.yo - this.world.sinZ * this.xo;
		var s  = this.world.cosY * this.zo + this.world.sinY * u;
		this.x = this.world.cosY * u - this.world.sinY * this.zo;
		this.y = this.world.sinX * s + this.world.cosX * t;
		this.z = this.world.cosX * s - this.world.sinX * t;

		// ---- 3D to 2D projection ----

		if (this.project2D) {
			this.visible = (this.world.zEye.value + this.z > 0);
			this.xp = canvas.centerX + this.x * (this.world.fl / (this.z + this.world.zEye.value));
			this.yp = canvas.centerY + this.y * (this.world.fl / (this.z + this.world.zEye.value));
		}

	};


	// set canvas

	var canvas = {  
		width:  0, 
		height: 0,
		elem: document.createElement('canvas'),
		resize: function () {
			var o = this.elem;
			var w = this.elem.offsetWidth * 1;
			var h = this.elem.offsetHeight * 1;
			if (w != this.width || h != this.height) {
				for (this.left = 0, this.top = 0; o != null; o = o.offsetParent) {
					this.left += o.offsetLeft;
					this.top  += o.offsetTop;
				}
				this.width = this.elem.width = w;
				this.height = this.elem.height = h;
				this.centerX = w / 2;
				this.centerY = h / 2;
				this.resize && this.resize();
			}
		},
		init: function () {
			var ctx = canvas.elem.getContext('2d');
			document.body.appendChild(canvas.elem);

			// jQuery('.efect2').prepend(canvas.elem);
			window.addEventListener('resize', canvas.resize.bind(canvas), true);
			return ctx;
		}
	};

	var ctx = canvas.init();

	// pointer

	var pointer = (function (canvas) {

		var started = false, endX = 0, endY = 0, scaling = false, oldDist = 0;
		var pointer = {
			x: 0,
			y: 0,
			dx: 0,
			dy: 0,
			startX: 0,
			startY: 0,
			canvas: canvas,
			touchMode: false,
			isDown: false,
			sweeping: false,
			scale: 0
		};

		// var distance = function (dx, dy) {
		// 	return Math.sqrt(dx * dx + dy * dy);
		// };
		
		// program events

		// [[window, 'mousemove,touchmove', function (e) {
		// 	this.touchMode = e.targetTouches;
		// 	if (this.touchMode) e.preventDefault();
		// 	if (scaling && this.touchMode) {
		// 		var d = distance(
		// 			this.touchMode[0].clientX - this.touchMode[1].clientX, 
		// 			this.touchMode[0].clientY - this.touchMode[1].clientY
		// 		);
		// 		var s = d > oldDist ? -0.4 : 0.4;
		// 		oldDist = d;
		// 		this.scale += s;
		// 		if (this.pinch) this.pinch(e);
		// 		return;
		// 	}
		// 	var pointer = this.touchMode ? this.touchMode[0] : e;
		// 	this.x = pointer.clientX - this.canvas.left;
		// 	this.y = pointer.clientY - this.canvas.top;
		// 	if (started) {
		// 		this.sweeping = true;
		// 		this.dx = endX - (this.x - this.startX);
		// 		this.dy = endY - (this.y - this.startY);
		// 	}
		// 	if (this.move) this.move(e);
		// }],
		// [canvas.elem, 'mousedown,touchstart', function (e) {
		// 	this.touchMode = e.targetTouches;
		// 	if (this.touchMode) e.preventDefault();
		// 	if (this.touchMode && e.touches.length === 2) {
		// 		scaling = true;
		// 		started = false;
		// 	} else {
		// 		var pointer = this.touchMode ? this.touchMode[0] : e;
		// 		this.startX = this.x = pointer.clientX - this.canvas.left;
		// 		this.startY = this.y = pointer.clientY - this.canvas.top;  
		// 		started = true;
		// 		this.isDown = true;
		// 		if (this.down) this.down(e);
		// 		setTimeout(function () {
		// 			if (!started && Math.abs(this.startX - this.x) < 11 && Math.abs(this.startY - this.y) < 11) {
		// 				if (this.tap) this.tap(e);
		// 			}
		// 		}.bind(this), 200);
		// 	}
		// }],
		// [window, 'mouseup,touchend,touchcancel', function (e) {
		// 	e.preventDefault();
		// 	if (scaling) {
		// 		scaling = false;
		// 		return;
		// 	}
		// 	if (started) {
		// 		endX = this.dx;
		// 		endY = this.dy;
		// 		started = false;
		// 		this.isDown = false;
		// 		if (this.up) this.up(e);
		// 		this.sweeping = false;
		// 	}
		// }]].forEach(function (e) {
		// 	for (var i = 0, events = e[1].split(','); i < events.length; i++) {
		// 		e[0].addEventListener(events[i], e[2].bind(pointer), false );
		// 	}
		// }.bind(pointer));

		window.addEventListener('wheel', wheel.bind(pointer), false);
		function wheel (e) {
			// var s = e.deltaY > 0 ? -1 : +1;
			this.scale += 0;
		}

		return pointer;

	}(canvas));

	// resize

	canvas.resize();

	var world  = new World (22, 200);
	world.build();


	run();

} ();