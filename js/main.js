var root = location.origin+'/';
var url = root + 'wp-content/themes/flatkingdon/img/';
function transY(arg1, arg2){
    arg1.css({
        'transform':'translateY(-'+arg2+'px)',
        '-webkit-transform':'translateY(-'+arg2+'px)',
        '-ms-transform':'translateY(-'+arg2+'px)',
        '-moz-transform':'translateY(-'+arg2+'px)'                
    });
}
(function($){

	$(document).ready(function(){

        $('.wpcf7-mailpoetsignup').trigger('click');

		
        // if( location.href == root || location.href == root+'en/' )
        // {
        //     $("#carousel").featureCarousel({
        //         trackerIndividual : false,
        //         trackerSummation : false,
        //         sidePadding : 150
        //     });    
        // }	

		/*-- googlemaps --*/
		function initialize() {

            var coordenadas = new google.maps.LatLng(21.010580, -89.587820);
            var marker;
            var colores = [
                {
                    featureType: "all",
                    elementType: "all",
                    stylers: [
                        { saturation: -100 }
                    ]
                }
            ];
            var opciones = {
                zoom: 16,
                center: coordenadas,
                mapTypeId: google.maps.MapTypeId.ROADMAP
                // panControl: false,
                // zoomControl: false,
                // scaleControl: false
            };

            var mapa = new google.maps.Map(document.getElementById('mapa'), opciones);

            var estilo = new google.maps.StyledMapType(colores);
            mapa.mapTypes.set('mapa-bn', estilo);
            mapa.setMapTypeId('mapa-bn');
            
            var marcador = new google.maps.Marker({
                position: coordenadas,
                map: mapa,
                icon : url+'/marker.png', //Marcador personalizado
                animation: google.maps.Animation.BOUNCE//Animación del marcador
            }); 
        }   
        if( location.href == root+'contacto/' || 
            location.href == root+'en/contact/')
        {
            google.maps.event.addDomListener(window, 'load', initialize);
        }
        
        /*-- googlemaps --*/
        $('#goconvocatoria').click(function(){
            window.href = root+'convocatoria';
        });
        
        function modlist(lic){
            var content2;
            for( var i = 0; i < lic.length; i++ )
            {   
                content2 = $(lic[i]).html();
                $(lic[i]).html('<span>'+content2+'</span>');
            } 
        }
        if( $('#descripcion')  ){
            modlist($('#descripcion ul li'));                    
        }

        if( $('body').find('#list') ){
            modlist($('#list ul li'));
            modlist($('#list4 ul li'));
        }
        jQuery('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = jQuery(this.hash);
                target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    jQuery('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
        
        $('#markright').on('click', function(){
            $('.gallery').flickity('next');
        });
        $('#markleft').on('click', function(){
            $('.gallery').flickity('previous');
        });
        
        // $('.off-canvas-list').find('li ul').addClass('left-submenu');
        // $('#menu-item-47').addClass('has-submenu');
        // $('#menu-item-143').addClass('has-submenu');

        // $('#menu-item-47 ul').append('<li class="back"><a href="#"></a></li>');
        // $('#menu-item-143 ul').append('<li class="back"><a href="#"></a></li>');
        if( location.href != root+'blog/' ) {
            $(window).scroll(function() {
                var y = $(window).scrollTop();
                y = y - 200;
                transY( $('.tizq') , y );
                transY( $('.tder') , y );
            });
        }
        $(document).foundation();
        $('#iconmovil').click(function(){
            $('.movil').toggleClass('oculto');
            $('.el').toggleClass('empuje');
            $('.telon').toggleClass('no-show');  
        });
        $('.telon').click(function(){
            $('.movil').toggleClass('oculto'); 
            $('.el').toggleClass('empuje');
            $(this).toggleClass('no-show'); 
            
        })
	});// document ready

})(jQuery);