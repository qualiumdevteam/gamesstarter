<?php get_header(); $cont=0;?>
<style>
	.tizq, .tder{top:110%;}
	input[type="checkbox"]{
    	display: none;
	}
</style>
<section class="single-proyecto">
<?php while ( have_posts() ) : the_post(); ?>
<?php 
	if( get_post_meta( $post->ID, 'pixelre', true ) ):
		echo get_post_meta( $post->ID, 'pixelre', true );
	endif;
 ?>
<?php //$img = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) );?>
<?php 
	$proyID = $post->ID;
	$url = get_template_directory_uri();
	$img = get_post_meta( $post->ID, 'background', true );
	$desarrollador = get_post_meta( $post->ID, 'urldesarrollador', true );
	$foot = get_post_meta( $post->ID, 'imgfooter', true );
	$logo = get_post_meta( $post->ID, 'logo', true );
	$video = get_post_meta( $post->ID, 'video', true );
	$descripcion = get_post_meta( $post->ID, 'descripcion', true );
	$formulario = get_post_meta( $post->ID, 'formulario', true );
	$slug = $post->post_name;
?>

	<section>
		<img class="imgdestacad" src="<?=$img?>">
		<!-- <div class="skew"></div> -->
		<div class="skew2"></div>
		<div class="row center">
			<div class="columns large-6 medium-6 small-12 contenidoproy">
				<br><br>
				<?php 						
					the_title('<h1 class="title">','</h1>');						 	
					the_content();						
				?>
			</div>
			<div id="descripcion" class="columns large-5 large-offset-1 medium-5 medium-offset-1 small-12">
				<?php 
					if(ICL_LANGUAGE_CODE == 'es'):
						echo '<h2 class="text-center">¡Descárgalo<br>antes que nadie!</h2>';
						echo do_shortcode($formulario);//do_shortcode( '[contact-form-7 id="52" title="proyectos"]' );
					else:
						echo '<h2 class="text-center">Download<br>before anyone else!</h2>';
						echo do_shortcode($formulario);//do_shortcode( '[contact-form-7 id="180" title="projects"]' );
					endif;
					echo $descripcion;
				?>				
			</div>
		</div>
	</section>
<?php $cont++; if($cont==1){break;} endwhile; ?>
	<section id="video">
		<iframe class="youtube-player" type="text/html" src="<?=$video?>?rel=0&vq=hd720" frameborder="0" allowfullscreen></iframe>
	</section>

	<section id="tips">
		<?php 		
			$cat = get_category_by_slug( $slug );
			$arg = array(
				'post_type'  => 'Testimonios',
				'post_status'=> 'publish',
				'cat'	 => $cat->term_id
				);
			$staments = new WP_Query($arg);
		?>
		<div class="row center">
			<div class="childtips columns large-8 large-offset-2">
				<?php 
					if($staments->have_posts()):
						while($staments->have_posts()): 
							$staments->the_post();
							the_content();
							echo '<br>';
						endwhile;
					endif;
				?>
			</div>			
		</div>
	</section>
	<?php 
		$gal = get_post_meta($proyID,'galeria',true); 
		if(!empty($gal)):
			$imgs=''; $itm =1;
			foreach(json_decode($gal) as $img):
				$imgs.="<img class='m$itm' src='$img->full'><div class='clearfix'></div>";
				$itm++;
			endforeach; 
		endif;
	?>
	<div class="tizq"><?=$imgs?></div>
	<div class="tder"><?=$imgs?></div>
	
	<footer style="background:url(<?=$foot?>)">
		<div class="capa">
			<div class="center text-center">
				<a href="<?=$desarrollador?>" style="color:#fff;" target="_blank">
				<?php if(ICL_LANGUAGE_CODE=='es'): ?>
					Videojuego realizado por:
				<?php else: ?>
					Video game made by:
				<?php endif; ?>
				<img src="<?=$logo?>"></a>
			</div>
		</div>
	</footer>
</section>
<?php get_footer() ?>