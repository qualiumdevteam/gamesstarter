<div class="header">
	<div class="caps"></div>
	<div class="row center">
		<div class="columns large-6 medium-6 small-12">
			<?php 
				echo ( ICL_LANGUAGE_CODE == 'es') ?
				'<h1>Recibe las últimas novedades de tus videojuegos favoritos</h1>':
				'<h1>Receive the latest news from your favorite video games</h1>';
			?>
		</div>
		<div class="columns large-6 medium-6 small-12 text-center">	
			<?php 
				$form = ( ICL_LANGUAGE_CODE == 'es' ) ? '[wysija_form id="1"]' : '[wysija_form id="2"]';
				echo do_shortcode($form);
			?>
		</div>
	</div>
</div>