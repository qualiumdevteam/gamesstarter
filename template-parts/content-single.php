<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package flatkingdon
 */
$options = '{"cellAlign":"left","freeScroll":true,"contain":true,"prevNextButtons":false,"pageDots":true}';
$url = get_template_directory_uri();
$foot = get_post_meta( $post->ID, 'imgfooter', true );
$logo = get_post_meta( $post->ID, 'logo', true );
include "headerblog.php"; ?>
<style>
	p a{
		display:block;
		text-align: center;
	}
</style>
<article id="post-<?php the_ID(); ?>" <?php post_class('row'); ?>>

	
	<div class="entry-content">
		<div class="columns large-8 medium-12 small-12 text-center">
			<img src="<?=wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) )?>">
			<header id="entry-header" class="entry-header">
				<?php the_title( '<h1 class="title">', '</h1>' ); ?>
				<!-- <p><?php //get_post_meta( get_the_ID(), 'subtitulo', true)?></p> -->
			</header><!-- .entry-header -->
			<div class="entry-meta">
				<span>Por <span class="verde autor"><?php the_author() ?></span></span>
				<!-- <span class="publicado"> -->
					Publicado en <span class="verde"><span style="text-transform:lowercase"><?=get_the_time('j \DE\ F \DE\ Y') ?></span></span>
				<!-- </span> -->
			</div><!-- .entry-meta -->
			<div class="text-justify">
				<?php the_content() ?>	
			</div>
				
		</div>
		<div class="columns large-4 medium-12 small-12">
		<h3 style="color: #01A28A;text-align:center;">Juegos Recomendados</h3>
		<ul class="small-block-grid-2 medium-block-grid-3 large-block-grid-1">
		<?php 
			$query = new WP_Query(array(
				'order'       => 'DESC',
				'post_type'   => 'proyectos'));				 
			
			while($query->have_posts()):
				$query->the_post(); 
	 			$img = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) ); 
	 	?>
	 		<li>
        		<a href="<?php the_permalink() ?>" class="cuadros">
          			<img class="carousel-image" alt="Image Caption" src="<?=$img?>">
          			<h4><?php the_title() ?></h4>
          		</a>    
        	</li>	
        <?php endwhile; wp_reset_query();?>  
        </ul>
			
		</div>
		
	</div><!-- .entry-content -->
</article><!-- #post-## -->
<div id="coments" class="row">
	<?php
		// If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;
	?>
</div>
<div class="row">
	<h4 class="artRel title">Artículos Relacionados</h4>
	<div class="gallery js-flickity" data-flickity-options='<?=$options?>'>
		<?php rel_posts() ?>		
	</div>
	<img id="markleft" src="<?=$url?>/img/arrow.png">
	<img id="markright" src="<?=$url?>/img/arrow.png">
</div>

