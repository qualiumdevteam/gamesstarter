<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package flatkingdon
 */

?>
<br><br>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		

		<?php if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php flatkingdon_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="columns large-3 medium-3 small-12" style="padding-left:0;">
		<img src="<?=wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) )?>">
	</div>
	<div class="columns large-9 medium-9 small-12 entry-summary">
		<?php the_title( sprintf( '<h3 class="title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>
		<?php the_excerpt(); ?>		
	</div><!-- .entry-summary -->
	<div class="clearfix"></div>

	<footer class="entry-footer">
		<?php flatkingdon_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

