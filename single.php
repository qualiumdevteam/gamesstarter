<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package flatkingdon
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="mainz" class="blog site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'template-parts/content', 'single' ); ?>

			<?php //the_post_navigation(); ?>
			
		<?php endwhile; // End of the loop. ?>
		<!-- <div class="row">
			<h1 class="title">Comentarios</h1>
		</div> -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
