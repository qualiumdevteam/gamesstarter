<?php
add_action( 'init', 'create_post_type' );
function create_post_type()
{
    register_post_type('Proyectos',
        array(
            'labels' => array(
                'name' => __('Proyectos'),
                'singular_name' => __('proyectos')                
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array('title','editor','thumbnail')
        )
    );
    register_post_type('Testimonios',
        array(
            'labels' => array(
            'name' => __('Testimonios'),
            'singular_name' => __('testimonio')
            ),
            'public' => true,
            'has_archive' => true,
            'taxonomies' => array('category'),
            'supports' => array('title','editor','thumbnail')
        )
    );
    /* post para la sección de nosotros */
    register_post_type('Equipo',
        array(
            'labels' => array(
            'name' => __('Equipo'),
            'singular_name' => __('equipo')
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array('title','editor','thumbnail')
        )
    );
}

function cyb_meta_box_background() {
    global $post;
    $background = get_post_meta($post->ID,'background',true);
    $background_p = (!empty($background)) ? $background : '';

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');
    $html = '<div>';
    $html .= "<input type='url' name='background' value='".$background_p."' placeholder='Pegue aquí la URL de la imagen que será el background' style='width:100%;heigth:35px;'>";
    $html .= '</div>';
    echo $html;
}
function cyb_meta_box_video() {
    global $post;
    $video = get_post_meta($post->ID,'video',true);
    $video_p = (!empty($video)) ? $video:'';
    
    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');
    $html = '<div>';
    $html .= "<input type='url' name='video' value='".$video_p."' placeholder='Pegue aquí la URL del Video promocional del videojuego' style='width:100%;heigth:35px;'>";
    $html .= '</div>';
    echo $html;
}
function cyb_meta_box_logo() {
    global $post;
    $logo = get_post_meta($post->ID,'logo',true);
    $logo_p =  (!empty($logo)) ? $logo : '' ;
    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');
    $html = '<div>';
    $html .= "<input type='url' name='logo' value='".$logo_p."' placeholder='Pegue aquí la URL del logotypo del desarrollador del videojuego' style='width:100%;heigth:35px;'>";
    $html .= '</div>';
    echo $html;
}
function cyb_meta_box_imgfooter() {
    global $post;
    $imgfooter = get_post_meta($post->ID,'imgfooter',true);
    $imgfooter_p =  (!empty($imgfooter)) ? $imgfooter : '' ;
    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');
    $html = '<div>';
    $html .= "<input type='url' name='imgfooter' value='".$imgfooter_p."' placeholder='Pegue aquí la URL de la imagen para el footer' style='width:100%;heigth:35px;'>";
    $html .= '</div>';
    echo $html;
}
add_action('admin_print_scripts', 'wp_fancy_commerce_admin_scripts');
function wp_fancy_commerce_admin_scripts(){
    wp_register_script('my-upload', get_template_directory_uri() .'/js/main-admin.js', array('jquery','media-upload','thickbox','wp-color-picker'));
    wp_enqueue_style('custom-galery-admin', get_template_directory_uri() .'/css/galery-admin.css');
    wp_enqueue_script('my-upload');
    wp_enqueue_script('jquery');
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    wp_enqueue_script( 'common' );    
}
function cyb_meta_box_galeria() {
    global $post;
    $songs = get_post_meta($post->ID,'galeria',true);
    if(!empty($songs)){
        $data = json_decode($songs);        
        $contador = 0;
        foreach($data as $img){
            echo "<div class='gaimg img_".$contador."'>";
            echo "<img src='".$img->full."' >";
            echo "<a data-position='".$contador."' class='remove_img' name='".$img->image."'>Borrar</a></div>";
            $contador++;
        }
        echo '<div style="clear:both"></div>';
    }
    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html = '<div style="clear: both;"></div>';
    $html .='<div class="img_gal">';
    $html .= '<div class="items">';
    $html .= '</div>';
    $html .= '<div style="clear: both;"></div>';
    $html .= "<input style='display:none;' id='image_location' type='text' name='galeria' value='".$songs."' size='50'>";
    $html .= '<input type="button" class="onetarek-upload-button button" id="wp_custom_attachment" data-contador="'.$contador.'" name="wp_custom_attachment" value="Agregar imagenes" />';
    $html .= '</div>';
    echo $html;
}
// postmeta para las convocatorias...
function cyb_meta_box_seccion2(){
    global $post;
    $seccion2 = get_post_meta($post->ID,'seccion2',true);
    $seccion2 = (!empty($seccion2)) ? $seccion2:'';
    wp_editor( $seccion2, 'seccion2', array('textarea_name'=>'seccion2'));    
}
function cyb_meta_box_seccion3(){
    global $post;
    $seccion3 = get_post_meta($post->ID,'seccion3',true);
    $seccion3 = (!empty($seccion3)) ? $seccion3:'';
    wp_editor( $seccion3, 'seccion3', array('textarea_name'=>'seccion3'));    
}
function cyb_meta_box_cuatroseccion(){
    global $post;
    $cuatroseccion = get_post_meta($post->ID,'cuatroseccion',true);
    $cuatroseccion = (!empty($cuatroseccion)) ? $cuatroseccion:'';
    wp_editor( $cuatroseccion, 'cuatroseccion', array('textarea_name'=>'cuatroseccion'));    
}
function cyb_meta_box_seccion5(){
    global $post;
    $seccion5 = get_post_meta($post->ID,'seccion5',true);
    $seccion5 = (!empty($seccion5)) ? $seccion5:'';
    wp_editor( $seccion5, 'seccion5', array('textarea_name'=>'seccion5'));    
}
function cyb_meta_box_seccion6(){
    global $post;
    $seccion6 = get_post_meta($post->ID,'seccion6',true);
    $seccion6 = (!empty($seccion6)) ? $seccion6:'';
    wp_editor( $seccion6, 'seccion6', array('textarea_name'=>'seccion6'));    
}
function cyb_meta_box_descripcion() {
    global $post;
    $descripcion = get_post_meta($post->ID,'descripcion',true);
    if(!empty($descripcion)){
        $descripcion_prod=$descripcion;
    }
    else{
        $descripcion_prod='';
    }
    wp_editor( $descripcion_prod, 'descripcion', array('textarea_name'=>'descripcion'));
}
function cyb_meta_box_urldesarrollador() {
    global $post;
    $urldesarrollador = get_post_meta($post->ID,'urldesarrollador',true);
    $urldesarrollador = (!empty($urldesarrollador)) ? $urldesarrollador : '';

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');
    $html = '<div>';
    $html .= "<input type='url' name='urldesarrollador' value='".$urldesarrollador."' placeholder='Pegue aquí la URL del desarrollador' style='width:100%;heigth:35px;'>";
    $html .= '</div>';
    echo $html;
}
// post_meta para el formulario de cada proyecto
function cyb_meta_box_formulario(){
    global $post;
    $formulario = get_post_meta($post->ID,'formulario',true);
    $formulario = (!empty($formulario)) ? $formulario : '';

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');
    $html = '<div>';
    $html .= "<input type='text' name='formulario' value='".$formulario."' placeholder='Pegue aquí el shortcode de su formulario' style='width:100%;heigth:35px;'>";
    $html .= '</div>';
    echo $html;
}
// Post meta para el pixel de remarketin
function cyb_meta_box_pixelderemarketing(){
    global $post;
    $pixelre = get_post_meta($post->ID,'pixelre',true);
    $pixelre = (!empty($pixelre)) ? $pixelre:'';
    wp_editor( $pixelre, 'pixelre', array('textarea_name'=>'pixelre'));    
}
add_action('add_meta_boxes', 'cyb_meta_boxes');
function cyb_meta_boxes() {

    add_meta_box( 
        'cyb-meta-box_descripcion'
        ,__('Descripción')
        ,'cyb_meta_box_descripcion'
        ,'proyectos'
    );
     add_meta_box( 
        'cyb-meta-box_galeria', 
        __('Invaders para los laterales'), 
        'cyb_meta_box_galeria', 
        'proyectos' 
    );
    /* meta_box´s para la el postype proyectos */
    add_meta_box( 
        'cyb-meta-box_formulario'
        ,__('Formulario de suscripción')
        ,'cyb_meta_box_formulario'
        ,'proyectos'
    );
    add_meta_box( 
        'cyb-meta-box_pixelderemarketing'
        ,__('Seleccione la opción texto del editor y pegue el Pixel de Remarketing')
        ,'cyb_meta_box_pixelderemarketing'
        ,'proyectos'
    );
    add_meta_box( 
        'cyb-meta-box-background', 
        __('Background de la primera sección del proyecto'), 
        'cyb_meta_box_background', 
        'proyectos' 
    );
    add_meta_box( 
        'cyb-meta-box-video', 
        __('Video'), 
        'cyb_meta_box_video', 
        'proyectos' 
    );
    add_meta_box( 
        'cyb-meta-box-logo', 
        __('Logo para el footer'), 
        'cyb_meta_box_logo', 
        'proyectos' 
    );
    add_meta_box( 
        'cyb-meta-box-imgfooter', 
        __('Imagen de background para el footer'), 
        'cyb_meta_box_imgfooter', 
        'proyectos' 
    );
    /* meta-box para las seccionas de las páginas del admin*/
    add_meta_box( 
        'cyb-meta-box_seccion2', 
        __('Seccion 2'), 
        'cyb_meta_box_seccion2', 
        'page' 
    );
    add_meta_box( 
        'cyb-meta-box_seccion3', 
        __('Seccion 3'), 
        'cyb_meta_box_seccion3', 
        'page' 
    );
    add_meta_box( 
        'cyb-meta-box_cuatroseccion', 
        __('Seccion 4'), 
        'cyb_meta_box_cuatroseccion', 
        'page' 
    );

    add_meta_box( 
        'cyb-meta-box_seccion5', 
        __('Seccion 5'), 
        'cyb_meta_box_seccion5', 
        'page' 
    );
    add_meta_box( 
        'cyb-meta-box_seccion6', 
        __('Seccion 6'), 
        'cyb_meta_box_seccion6', 
        'page' 
    );  
    add_meta_box( 
        'cyb-meta-box_urldesarrollador', 
        __('Página web del Desarrollador'), 
        'cyb_meta_box_urldesarrollador', 
        'proyectos' 
    ); 
    
}

add_action( 'save_post', 'dynamic_save_postdata' );

function dynamic_save_postdata() {

    global $post;
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
       return;
    // if ( !isset( $_POST['dynamicMeta_noncename2'] ) )
    //     var_dump($_POST); die();
    // if ( !wp_verify_nonce( $_POST['dynamicMeta_noncename2'], plugin_basename( __FILE__ ) ) )
    // return;
   
    if(isset($_POST['background'])){
        update_post_meta($post->ID,'background',$_POST['background']);    
    }
    if(isset($_POST['descripcion'])){
        update_post_meta($post->ID,'descripcion',$_POST['descripcion']);
    }
    if(isset($_POST['imgfooter'])){
        update_post_meta($post->ID,'imgfooter',$_POST['imgfooter']);
    }
    if(isset($_POST['logo'])){
        update_post_meta($post->ID,'logo',$_POST['logo']);
    }
    if(isset($_POST['video'])){
        update_post_meta($post->ID,'video',$_POST['video']);
    }

    // Galería de proyectos
    if(isset($_POST['galeria'])){
        update_post_meta($post->ID,'galeria',$_POST['galeria']);    
    }
    // shortcode para el formulario de cada proyecto
    if(isset($_POST['formulario'])){
        update_post_meta($post->ID,'formulario',$_POST['formulario']);    
    }
    // pixel de remarketing
    if(isset($_POST['pixelre'])){
        update_post_meta($post->ID,'pixelre',$_POST['pixelre']);    
    }
    // Secciones de los post_type page
    if(isset($_POST['seccion2'])){
        update_post_meta($post->ID,'seccion2',$_POST['seccion2']);    
    }

    if(isset($_POST['seccion3'])){        
        update_post_meta($post->ID,'seccion3',$_POST['seccion3']);
    }
    if(isset($_POST['cuatroseccion'])){
        update_post_meta($post->ID,'cuatroseccion',$_POST['cuatroseccion']);
    }
    if(isset($_POST['seccion5'])){
        update_post_meta($post->ID,'seccion5',$_POST['seccion5']);
    }
    if(isset($_POST['seccion6'])){
        update_post_meta($post->ID,'seccion6',$_POST['seccion6']);
    }
    if(isset($_POST['urldesarrollador'])){
        update_post_meta($post->ID,'urldesarrollador',$_POST['urldesarrollador']);
    }
}